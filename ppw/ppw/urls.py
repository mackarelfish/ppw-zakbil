from django.contrib import admin
from django.urls import path, include

from .views import index, time

urlpatterns = [
    path('', index),
    path('time/', time),
    path('time/<int:offset>', time),
    path('story1/', include("story1.urls", namespace="story1")),
    path('story3/', include("story3.urls", namespace="story3")),
    path('story5/', include("story5.urls", namespace="story5")),
    path('story6/', include("story6.urls", namespace="story6")),
    path('admin/', admin.site.urls),
]
