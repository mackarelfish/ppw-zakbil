from django.shortcuts import render
from dateutil.parser import parse
import datetime

# Create your views here.
def index(request):
  return render(request, "index.html")

def time(request, offset=0):
    tzobj = datetime.datetime.now(datetime.timezone.utc).astimezone()
    info = tzobj.tzinfo
    context = {
        "value": tzobj,
    }
    return render(request, "time.html", context=context)