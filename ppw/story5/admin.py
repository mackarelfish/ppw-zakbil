from django.contrib import admin
from .models import Kelas, Tugas

# Register your models here.
admin.site.register(Kelas)
admin.site.register(Tugas)
