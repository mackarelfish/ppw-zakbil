from django.db import models
from django.forms import ModelForm
from django import forms

from .customfields import RangedIntegerField

class Kelas(models.Model):
    nama = models.CharField(blank=False, max_length=100)
    dosen = models.CharField(max_length=100, blank=False)
    sks = RangedIntegerField(blank=False, min_value=1, max_value=6)
    deskripsi = models.TextField()
    semester = models.CharField(max_length=20, blank=False)
    kelas = models.CharField(max_length=20, blank=False)

    def __str__(self):
        return f"<{self.nama}>"

class Tugas(models.Model):
    kelas = models.ForeignKey('Kelas', on_delete=models.CASCADE)
    nama = models.CharField(max_length=20, blank=False)
    deskripsi = models.TextField()
    deadline = models.DateTimeField()

class TugasForm(ModelForm):
    model = Tugas
    fields = ['nama', 'deskripsi', 'deadline']

class KelasForm(ModelForm):
    class Meta:
        model = Kelas
        fields = ['nama', 'dosen', 'sks', 'deskripsi', 'semester', 'kelas']

    def clean_nama(self):
        data = self.cleaned_data.get('nama')
        if Kelas.objects.filter(nama=data):
            raise forms.ValidationError(f"The class {data} is already registered.")
        return data
