from django.urls import path, re_path
from .views import kelas, index, kelas_s

app_name="story5"

urlpatterns = [
    path('api/kelas/<int:id>', kelas),
    path('api/kelas/', kelas),
    path('', index, name="index"),
    path('<int:id>/', kelas_s),
]