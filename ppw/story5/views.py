from django.views.decorators.http import require_http_methods
from django.core.serializers import serialize
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
import json

from .models import Kelas, KelasForm
from .encoder import ExtendedEncoder

# Create your views here.
@require_http_methods(["GET", "POST", "DELETE"])
@csrf_exempt
def kelas(request, id=None):
    # @POST
    if request.method == "POST":
        try:
            res = json.loads(request.body)
            if Kelas.objects.filter(nama=res['nama']):
                raise Exception(f"The class {res['nama']} is already registered") 
            newKelas = Kelas(**res)
            newKelas.save()

            newKelas = json.loads(serialize('json', [newKelas]))[0]
            res = {"success": True, "data": {"pk": newKelas['pk'], "fields": newKelas['fields']}}
        except Exception as e:
            res = {"success": False, "error": str(e)}

        return JsonResponse(res, safe=False, json_dumps_params={'indent': 2})

    # @DELETE
    if request.method == "DELETE":
        res = {}
        if not id:
            res['success'] = False
            res['error'] = "No id specified"
        else:
            getKelas = Kelas.objects.filter(pk=id)
            if not getKelas:
                res['success'] = False
                res['error'] = "No class with that id"
            else:
                res['success'] = True
                res['data'] = list(getKelas)
                getKelas.delete()

        return JsonResponse(res, safe=False, encoder=ExtendedEncoder, json_dumps_params={"indent": 2})

    # @GET
    if id:
        getData = Kelas.objects.filter(pk=id)
        newData = json.loads(serialize('json', getData))
        for idx in range(len(getData)):
            data = json.loads(serialize('json', list(getData[idx].tugas_set.all())))
            for i in range(len(data)):
                data[i] = data[i]['fields']
            newData[idx]['fields']['tugas'] = data
            newData[idx]['fields']['id'] = newData[idx]['pk']
            newData[idx] = newData[idx]['fields']

        if not list(getData):
           return JsonResponse({"success": False, "error": "No class with that id"}, json_dumps_params={'indent': 2}) 
    else:
        getData = list(Kelas.objects.all())
        newData = json.loads(serialize('json', getData))
        for idx in range(len(getData)):
            data = json.loads(serialize('json', list(getData[idx].tugas_set.all())))
            for i in range(len(data)):
                data[i] = data[i]['fields']
            newData[idx]['fields']['tugas'] = data
            newData[idx]['fields']['id'] = newData[idx]['pk']
            newData[idx] = newData[idx]['fields']
    return JsonResponse({"success": True, "data": newData}, safe=False, encoder=ExtendedEncoder, json_dumps_params={"indent": 2})


def index(request):
    form = KelasForm()
    r  = requests.get("https://ppw-zakbil.herokuapp.com/story5/api/kelas")
    if request.method == "POST":
        form = KelasForm(request.POST)
        if form.is_valid():
            form.save()

    context = {
        "data": r.json(),
        "form": form
    }

    return render(request, "story5/index.html", context)
def kelas_s(request, id):
    if id:
        r = requests.get(f"https://ppw-zakbil.herokuapp.com/story5/api/kelas/{id}")
    else:
        redirect("story5.index")

    return render(request, 'story5/kelas.html', {"data": r.json()})