# Generated by Django 3.0.3 on 2020-03-06 11:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('story6', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='peserta',
            name='kegiatan',
        ),
        migrations.AddField(
            model_name='kegiatan',
            name='peserta',
            field=models.ManyToManyField(to='story6.Peserta'),
        ),
    ]
