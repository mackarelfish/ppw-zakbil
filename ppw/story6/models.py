from django.db import models
from django.forms import ModelForm


class Peserta(models.Model):
    nama = models.CharField(max_length=20)


class Kegiatan(models.Model):
    nama = models.CharField(max_length=20)
    desk = models.TextField()
    peserta = models.ManyToManyField(Peserta, blank=True)


class KegiatanForm(ModelForm):
    class Meta:
        model = Kegiatan
        fields = ['nama', 'desk']
