from django.test import TestCase, Client
from django.urls import resolve
from .models import Kegiatan, Peserta
from .views import index


# Create your tests here.
class Story6UnitTest(TestCase):
    def test_story6_url_exist(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)


    def test_story6_model_create(self):
        x = Kegiatan(nama="Budi", desk="aaaa")
        x.save()
        jumlah = Kegiatan.objects.all().count()
        self.assertEqual(jumlah, 1)


    def test_story6_template_used(self):
        response = Client().get('/story6/')
        self.assertTemplateUsed(response, 'story6/index.html')


    def test_story6_using_index(self):
        response = resolve('/story6/')
        self.assertEqual(response.func, index)
