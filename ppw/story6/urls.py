from django.urls import path
from . import views

app_name = "story6"

urlpatterns = [
    path('', views.index, name="index"),
    path('remove/<int:id>', views.remove, name="remove"),
    path('tambah/', views.tambahKegiatan, name="tambah")
]
