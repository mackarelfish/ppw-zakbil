from django.shortcuts import render, redirect
from .models import Kegiatan, Peserta, KegiatanForm


def index(request):
    getKegiatan = Kegiatan.objects.all()

    if request.method == "POST":
        keg = Kegiatan.objects.filter(pk=int(request.POST['id'])).first()
        nam = Peserta.objects.filter(nama=request.POST['nama'])
        if not nam:
            x = Peserta(nama=request.POST['nama'])
            x.save()
            keg.peserta.add(x)
            keg.save()
        else:
            keg.peserta.add(nam.first())
            keg.save()
        redirect('story6.views.index')

    context = {
       "data": getKegiatan,
    }

    return render(request, 'story6/index.html', context)


def remove(request, id):
    rem = Peserta.objects.filter(pk=id).first()
    kegi = request.GET.get('kegi', '')

    keg = Kegiatan.objects.filter(pk=int(kegi)).first()
    keg.peserta.remove(rem)
    keg.save()

    return redirect('story6:index')


def tambahKegiatan(request):
    getKegiatan = Kegiatan.objects.all()
    form = KegiatanForm()

    if request.method == "POST":
        form = KegiatanForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/story6/')

    context = {
       "data": getKegiatan,
       "form": form
    }

    return render(request, 'story6/index.html', context)
